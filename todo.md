
- remove old plots
- edit sync data

- replace _S1_ rds
  - more data cleaning is needed
- add Legacy export R30 to run in CM_21_GLB workflow
  - rename rds files from legacy to operational

- use/check Async step data

- missing inclined files may be in radmon folder
  - binary files exist in sirena

